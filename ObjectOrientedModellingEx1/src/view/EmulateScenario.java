package view;

import model.Employee;
import model.Product;
import java.util.Scanner;

public class EmulateScenario {

    public static void initializeApplication() {
        Employee employee1 = new Employee("George", "Employee", 20);
        Product product1 = new Product(10.5, "BrandOne", 10);
        Product product2 = new Product(20.5, "BrandTwo", 20);
        Product product3 = new Product(2.2, "BrandThree", 3);
        employee1.getProductList().add(0, product1);
        employee1.getProductList().add(1, product2);
        employee1.getProductList().add(2, product3);
        Scanner sc = new Scanner(System.in);
        System.out.println("Hello!");
        System.out.println("Our products are :");
        System.out.println(employee1.getProductList());
        System.out.println("Choose a product you want to buy");
        System.out.println("Submit its name");
        String c = sc.next();
        if (c.equals("BrandOne")) {
            product1.setQuantity(product1.reduceQuantity(product1.getQuantity()));
            if (product1.getQuantity() <= 0) {
                System.out.println("This product isn't available anymore!");
            } else {
                System.out.println("Product bought!");
            }
        }
        if (c.equals("BrandTwo")) {
            product2.setQuantity(product2.reduceQuantity(product2.getQuantity()));
            if (product2.getQuantity() <= 0) {
                System.out.println("This product isn't available anymore!");
            } else {
                System.out.println("Product bought!");
            }
        }
        if (c.equals("BrandThree")) {
            product3.setQuantity(product3.reduceQuantity(product3.getQuantity()));
            if (product3.getQuantity() <= 0) {
                System.out.println("This product isn't available anymore!");
            } else {
                System.out.println("Product bought!");
            }
        }
        System.out.println("New quantities for the above products are :");
        System.out.println("Quantity for the first one is " + product1.getQuantity());
        System.out.println("Quantity for the first two is " + product2.getQuantity());
        System.out.println("Quantity for the first three is " + product3.getQuantity());
    }

}
