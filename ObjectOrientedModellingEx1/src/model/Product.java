package model;

public class Product {

    private double price;
    private String brand;
    private int quantity;

    public Product(double price, String brand, int quantity) {
        this.price = price;
        this.brand = brand;
        this.quantity = quantity;
    }

    public int reduceQuantity(int quantity) {
        quantity = quantity - 1;
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                ", brand='" + brand + '\'' +
                ", quantity=" + quantity +
                '}';
    }

}
